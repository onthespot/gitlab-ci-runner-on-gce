sudo apt-get update

sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

mkdir -p /opt/minio
curl --output /opt/minio/minio https://dl.minio.io/server/minio/release/linux-amd64/minio
chmod 755 /opt/minio/minio

cat > /etc/systemd/system/minio.service <<- EOF

${minio_service}

EOF
chmod 0600 /etc/systemd/system/minio.service
systemctl enable minio
systemctl start minio


mkdir -p /etc/gitlab-runner
cat > /etc/gitlab-runner/config.toml <<- EOF

${runners_config}

EOF

curl -L https://dl.google.com/cloudagents/install-logging-agent.sh | sudo bash

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

curl -L https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-$(uname -s)-$(uname -m) > /tmp/docker-machine && sudo install /tmp/docker-machine /usr/local/bin/docker-machine

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get -y install gitlab-runner