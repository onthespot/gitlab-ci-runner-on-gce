[Unit]
Description=Minio
Documentation=http://minio.io
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Environment=MINIO_ACCESS_KEY=${minio_access_key}
Environment=MINIO_SECRET_KEY=${minio_secret_key}
Restart=on-failure
ExecStart=/opt/minio/minio gateway gcs ${project}
KillSignal=SIGINT
ExecReload=/bin/kill -HUP $MAINPID

[Install]
WantedBy=multi-user.target