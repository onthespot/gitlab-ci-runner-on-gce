concurrent = 16
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "${runner_name}"
  url = "${runner_url}"
  token = "${runner_token}"
  executor = "docker+machine"
  environment = ["DOCKER_TLS_CERTDIR=", "DOCKER_DRIVER=overlay2"]
  [runners.docker]
    tls_verify = false
    image = "busybox"
    privileged = true
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    Type = "s3"
    Shared = true
    [runners.cache.s3]
      ServerAddress = "${name}:9000"
      AccessKey = "${minio_access_key}"
      SecretKey = "${minio_secret_key}"
      BucketName = "${bucket_name}"
      Insecure = true
  [runners.machine]
    IdleCount = ${runner_idle_count}
    IdleTime = ${runner_idle_time}
    MachineDriver = "google"
    MachineName = "s-%s"
    MachineOptions = [
      "engine-registry-mirror=https://mirror.gcr.io",
      "google-project=${project}",
      "google-zone=${zone}",
      "google-network=${network}",
      "google-subnetwork=${subnetwork}",
      "google-machine-type=${runner_machine_type}",
      "google-machine-image=${runner_machine_image}",
      "google-preemptible=true",
      "google-use-internal-ip-only=true",
      "google-disk-size=10",
      "google-service-account=${runner_service_account}"
    ]