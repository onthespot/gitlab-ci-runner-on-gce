/* = Provider ================================= */

provider "google" {
  project = "${var.project}"
  region  = "${var.region}"
}

locals {
  service_account_iam_roles = [
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/compute.admin",
    "roles/storage.admin",
    "roles/iam.serviceAccountUser"
  ]

  worker_sa_iam_roles = [
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter"
  ]
}

resource "google_compute_network" "default" {
  name                    = "${var.name}"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "default" {
  name                     = "${var.name}"
  ip_cidr_range            = "${var.ip_cidr_range}"
  network                  = "${google_compute_network.default.self_link}"
  region                   = "${var.region}"
  private_ip_google_access = true
}

resource "google_compute_router" "default" {
  name    = "${var.name}"
  region  = "${var.region}"
  network = "${google_compute_network.default.self_link}"
}

resource "google_compute_router_nat" "default" {
  name                               = "${var.name}"
  router                             = google_compute_router.default.name
  region                             = var.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

resource "google_compute_firewall" "default" {
  name    = "docker-machines"
  network = "${google_compute_network.default.name}"

  allow {
    protocol = "tcp"
    ports    = ["22", "2376"]
  }

  source_tags = ["gitlab-runner"]
  target_tags = ["docker-machine"]
}

resource "google_compute_firewall" "minio" {
  name    = "minio"
  network = "${google_compute_network.default.name}"

  allow {
    protocol = "tcp"
    ports    = ["9000"]
  }

  source_tags = ["docker-machine"]
  target_tags = ["gitlab-runner"]
}

resource "random_id" "ci-cache-name" {
  prefix      = "gitlab-ci-cache-"
  byte_length = 2
}

resource "google_storage_bucket" "ci-cache" {
  name     = "${random_id.ci-cache-name.hex}"
  location = "${var.region}"
}

resource "random_id" "minio_access_key" {
  byte_length = 4
}

resource "random_id" "minio_secret_key" {
  byte_length = 16
}

resource "google_service_account" "gitlab-ci-runner" {
  account_id   = "gitlab-ci-runner"
  display_name = "Gitlab CI Runner"
}

resource "google_project_iam_member" "gitlab-ci-runner-iam" {
  count  = length(local.service_account_iam_roles)
  role   = element(local.service_account_iam_roles, count.index)
  member = "serviceAccount:${google_service_account.gitlab-ci-runner.email}"
}

resource "google_service_account" "gitlab-ci-worker" {
  account_id   = "gitlab-ci-worker"
  display_name = "Gitlab CI Worker"
}

resource "google_project_iam_member" "gitlab-ci-worker-iam" {
  count  = length(local.worker_sa_iam_roles)
  role   = element(local.worker_sa_iam_roles, count.index)
  member = "serviceAccount:${google_service_account.gitlab-ci-worker.email}"
}

data "template_file" "runner_config" {
  template = "${file("${path.module}/templates/config.toml.tpl")}"

  vars = {
    project                = "${var.project}"
    zone                   = "${var.zone}"
    network                = "${google_compute_network.default.name}"
    subnetwork             = "${google_compute_subnetwork.default.name}"
    name                   = "${var.name}"
    bucket_name            = "${google_storage_bucket.ci-cache.name}"
    minio_access_key       = "${random_id.minio_access_key.hex}"
    minio_secret_key       = "${random_id.minio_secret_key.hex}"
    runner_name            = "${var.runner_name}"
    runner_token           = "${var.runner_token}"
    runner_url             = "${var.runner_url}"
    runner_idle_count      = "${var.runner_idle_count}"
    runner_idle_time       = "${var.runner_idle_time}"
    runner_machine_type    = "${var.runner_machine_type}"
    runner_machine_image   = "${var.runner_machine_image}"
    runner_service_account = "${google_service_account.gitlab-ci-worker.email}"
  }
}

data "template_file" "minio_service" {
  template = "${file("${path.module}/templates/minio.service.tpl")}"

  vars = {
    project          = "${var.project}"
    minio_access_key = "${random_id.minio_access_key.hex}"
    minio_secret_key = "${random_id.minio_secret_key.hex}"
  }
}

data "template_file" "startup" {
  template = "${file("${path.module}/templates/startup.sh.tpl")}"

  vars = {
    minio_service  = "${data.template_file.minio_service.rendered}"
    runners_config = "${data.template_file.runner_config.rendered}"
  }
}

resource "google_compute_instance" "default" {
  name         = "${var.name}"
  machine_type = "g1-small"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.default.self_link}"
  }

  tags = ["gitlab-runner"]

  metadata_startup_script = "${data.template_file.startup.rendered}"

  service_account {
    email = google_service_account.gitlab-ci-runner.email
    scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_write",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/trace.append",
    ]
  }

  allow_stopping_for_update = true
}