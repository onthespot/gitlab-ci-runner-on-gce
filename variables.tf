variable "project" {
  type = "string"
}

variable "region" {
  type    = "string"
  default = "europe-west1"
}

variable "zone" {
  type    = "string"
  default = "europe-west1-b"
}

variable "name" {
  type    = "string"
  default = "gitlab-ci-runner"
}

variable "ip_cidr_range" {
  type    = "string"
  default = "10.5.0.0/20"
}

variable "runner_name" {
  type = "string"
}

variable "runner_token" {
  type = "string"
}

variable "runner_url" {
  type    = "string"
  default = "https://gitlab.com/"
}

variable "runner_idle_count" {
  type    = "string"
  default = "0"
}

variable "runner_idle_time" {
  type    = "string"
  default = "0"
}

variable "runner_machine_type" {
  type    = "string"
  default = "n1-highcpu-4"
}

variable "runner_machine_image" {
  type    = "string"
  default = "coreos-cloud/global/images/family/coreos-stable"
}
